<!---
merge repo from gitlab to github: https://steveperkins.com/migrating-projects-from-github-to-gitlab/
I have used 2nd step over here so that i could update my search engine repo on both github and gitlab
at once.
-->

# Search Engine For E-Commerce
This project report is based on the search engine we designed and built for Ecommerce and Ecommerce for food . The students had to design and built it from scratch using multiple topics studied in Information Retrieval class at UTD under ** Prof. Sanda Harabagiu **. 

This project has six main components.
   1. Crawling (General E-Commerce)
   2. Crawling (E-Commerce for food)
   3. Indexing & Searching (General E-Commerce)
   4. Indexing & Searching (E-Commerce for food)
   5. UI Design (Common for both )
   6. Query Expansion

** note ** : I (Meet Shah) have worked on E-Commerce module therefore this repository will only contain the code related to E-Commerce. 

# Roles and Responsiblity

| Module Name           |            Topic Focus                    |   Contributor     | Contact Info                                                  |
| :-:                   |   :-:                                     |   :-:             | :-:                                                           |
| Crawling              |   General E-Commerce                      |   Yash Zalavadiya | [Github Profile](https://github.com/Yash-Z)                   |
| Crawling              |   E-Commerce for food                     |   Sharayu Sharad  | #                                                             |
| Indexing & Searching  |   General E-Commerce                      |   Meet Shah       | [Gitlab Profile](https://gitlab.com/users/slim-shah/projects) |
| Indexing & Searching  |   E-Commerce for food                     |   Siddhant Sahu   | #                                                             |
| UI Design             |   Common module for both of the topics    |   Charles         | [Github Profile](https://github.com/CharlesG12)               |
| Query Expansion       |   Common module for both of the topics    |   Manik Narang    | [Github Profile](https://github.com/manik0493)                |

# Dependencies for running this repo
Here is the list of dependencies that you will need to install on your computer to run this code on your machine.

   | Sr.No | Dependency  | Usage  |
   | :---: | :-: | :-: |
   | 1. | Node.js | For running front-end server |
   | 2. | Intellij IDE | Pls note that backend server will not run on anyother IDE apart from this. |
   | 3. |  JAVA 8 | - |



# Getting Started
Clone this repo on your local machine and unzip it. Navigate inside the root of unzipped directory and you will find there two more directories. Unzip both of these directories.

   1.  Back-End.zip
   2. front-end.zip
   
   ### 1.  Running Back-End
   
   Extract the zip file and import the project in IntelliJ as a Maven project. Note that we haven't tested on Eclipse.

  - For generating the index, run IndexFiles.java with the command line arguments:
   			-docs "path_to_parsed_documents_folder" -metadata "path_to_metadata_json_file"
        + ##### Parsed documents should be in a data folder: parsed
        + ##### Metadata in the same folder: metadata.json
  - In Search.java, modify your query and option in the main() function, run the application to check the results on the console.
  - Running Application.java starts the backend server.

  ### 2. Running Front-end
		Open terminal and type in following commands according to your Usage.
		##### install dependencies
			npm install

		##### serve with hot reload at localhost:8080
			npm run dev

		##### build for production with minification
			npm run build
For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

# Architecture

Following architecture gives the brief overview of our project. To get more detail information like: how did we integrate Pageranking with VectorSpace Model or How did we integarted QueryExpansion module with searching module please read our [Project Report](https://gitlab.com/slim-shah/searchengine/blob/master/ProjectReport.pdf).

![](/ScreenShots/mainArchitecture.jpeg?raw=true "Optional Title")

# ScreenShots
## Home Page
![](/ScreenShots/MainScreen.jpg?raw=true "Optional Title")
## Testing the SearchEngine by using search query as: "mobile cover" & selecting Rochio as Query Expansion method.
![](/ScreenShots/searchResults.jpg?raw=true "Optional Title")
## Testing the SearchEngine by using searchquery as: "women's shirt" & selecting Association Clustering as Query Expansion method.
![](/ScreenShots/searchResults1.jpg?raw=true "Optional Title")